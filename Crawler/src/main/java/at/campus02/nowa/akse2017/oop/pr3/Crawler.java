package at.campus02.nowa.akse2017.oop.pr3;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {

	public static void main(String[] args) {
		for (String s : args) {
			System.out.println(s);
			URL url = null;
			try {
				url = new URL(s);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}
			try (InputStream ins = url.openConnection().getInputStream()) {
				Document doc = Jsoup.parse(ins, "utf-8", "");
				Elements elems = doc.getElementsByTag("h2");
				for (Element e : elems) {
					System.out.println(e.text());
					System.out.println(e.nextElementSibling().text());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
