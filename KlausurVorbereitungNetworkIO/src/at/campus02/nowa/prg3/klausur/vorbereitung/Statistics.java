package at.campus02.nowa.prg3.klausur.vorbereitung;

public class Statistics {
	private int summe = 0;
	private int counter = 0;
	
	public void add(int i) {
		summe += i;
		counter++;
	}
	
	public float getAverage() {
		return (float)summe / counter;
	}

}
